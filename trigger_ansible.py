from datetime import datetime
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.bash_operator import BashOperator

def print_hello():
    return 'Hello world!'

dag = DAG('trigger_ansible', description='trigger an ansible playbook',
          schedule_interval='* * * * *',
          start_date=datetime(2022, 8, 10), catchup=False)


run_playbook = BashOperator(
    task_id='bash_get_path',
    bash_command='ansible-playbook /home/francisco/Documentos/Ansible/prueba/playbook.yml',
    dag=dag
)

dummy_operator = DummyOperator(task_id='dummy_task', retries=3, dag=dag)
dummy_operator >> run_playbook


